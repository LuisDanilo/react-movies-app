import React, {Component} from 'react'
import Movie from '../Movie/Movie'

class MoviesList extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }
  
  render() {
    let {movies} = this.props
    return (
      <div className="movies-list">
        {movies.map(movie => {
          return(
            <div key={movie.imdbID} className="movie-list-item">
              <Movie 
                title={movie.Title}
                year={movie.Year}
                poster={movie.Poster}/>
            </div>
          )
        })}
      </div>
    )
  }
}

export default MoviesList