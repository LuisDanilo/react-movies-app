import React from 'react'

const Title = ({children}) => {
  return (
    <h1 className='title'>{children}</h1>
  )
}

Title.defaultProps = {
  title: 'Default title'
}

export default Title