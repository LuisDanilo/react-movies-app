import React, {Component} from 'react'

const API_KEY = '4c0497c5'

class SearchForm extends Component {

  constructor(props) {
    super(props)
    this.state = {
      inputMovie: ''
    }
  }

  inputChanged = e => {
    this.setState({inputMovie: e.target.value})
  }

  submit = e => {
    e.preventDefault()
    const {inputMovie} = this.state
    fetch(`http://www.omdbapi.com/?apikey=${API_KEY}&s=${inputMovie}`)
      .then(res => res.json())
      .then(results => {
        let {Search, totalResults} = results
        this.props.onResults(Search, totalResults)
      })
  }

  render() {
    return (
      <form onSubmit={this.submit}>
        <div className="field has-addons">
          <div className="control">
            <input 
              className="input" 
              onChange={this.inputChanged}
              placeholder="Input"
              type="text"/>
          </div>
          <div className="control">
            <button className="button is-info">Search</button>
          </div>
        </div>
      </form>
    )
  }
}

export default SearchForm