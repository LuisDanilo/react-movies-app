import React, { Component } from 'react'
import 'bulma/css/bulma.css'
import './App.css'
import Title from './components/Title/Title'
import SearchForm from './components/SearchForm/SearchForm'
import MoviesList from './components/MoviesList/MoviesList'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      results: [],
      total: 0
    }
  }

  handleApiResponse = (results, total) => {
    // Update state
    this.setState({results, total})
  }

  render() {
    console.log(`RENDER:`)
    console.log(this.state)
    return(
      <div className="App">
        <header className="App-header">
          <Title>Buscar Peliculas</Title>
          <div className="form-container">
            <SearchForm onResults={this.handleApiResponse}/>
          </div>
          <hr/>
          {this.state.total === 0
            ? <p>Sin resultados</p>
            : <MoviesList movies={this.state.results}/>
          }
        </header>
      </div>
    )
  }
}

export default App;
